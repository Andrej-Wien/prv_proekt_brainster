<?php
require("pdo.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Dashboard</title>
</head>
<body>
 
<?php


//var_dump($conn);
$query = $conn->query("SELECT vrabotuvanje.*, typestudent.student_type FROM `vrabotuvanje`
LEFT JOIN typestudent ON vrabotuvanje.typestudent_id = typestudent_id");
// $studenti = $query->fetch();

// print_r($query->rowCount());
// print_r($studenti);
?>

<div class="container mt-4">
<h1 class="mb-3 text-center">Aplikanti</h1>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Company name</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
      <th scope="col">Student type</th>
      <th scope="created_at">Created on</th>
    </tr>
  </thead>
  <tbody>
      <?php
        if($query->rowCount() == 0) {
            echo "<tr><td colspan='6'>Nema aplikanti</td></tr>";
        }

        if($query->rowCount() > 0 ){ 
            while($row = $query->fetch()) {  ; ?>
            
            <tr>
                <th scope="row"><?php echo $row['id'] ?></th>
                <td><?php echo $row['name'] ?></td>
                <td><?php echo $row['companyName']?></td>
                <td><?php echo $row['email']?></td>
                <td><?php echo $row['tel']?></td>
                <td><?php echo $row['typestudent_id']?></td>
                <td><?php echo $row['created_at']?></td>
                </tr>
          
                <?php  }
                 }
                ?>
     
   
  </tbody>
</table>





</div>






<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

</body>
</html>

